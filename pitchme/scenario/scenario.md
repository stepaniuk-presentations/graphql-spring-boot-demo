# Intro
The main goal of this presentation is to give you a grasp of such technology as GraphQL. 

I hope that after this presentation you will have some understanding of its basic concepts, why this technology emerged 
and what problems it tries to solve.

# Introduction
## What is GraphQL
```
GraphQL is a query language for APIs and a runtime for fulfilling those queries.
```
So basically it's a specification on how you define the way of communication between web services. 
That means that it can be implemented in any programming language. 
As long as the implementation parses queries, schema, etc in the specified way, it will play nice with any other GraphQL 
application.
The main feature of this spec is that it tries reduce these complexities by arranging back-end data in graphical view.
Then client can query any fragment of this data, passing their query & get only what’s required.

```
Developed internally by Facebook in 2012 before being publicly released in 2015. In 2018, the GraphQL project was moved 
from Facebook to the newly-established GraphQL Foundation
```
So it is relatively new technology which became more and more popular and is taking its place in the industry.
For example Postman provided support for GraphQL only at the end of the last year(2019).

Notable companies employing the GraphQL standard include: Facebook, Twitter, Airbnb, AWS, IBM, PayPal and many more.

## Brief overview and short demo
I'd like to demonstrate a brief overview of GraphQL so that it's easier to imagine and understand what I'm going to talk
about further. For this purposes I have a simple server application which is running locally on my machine and 
I'm gonna use a tool called GraphiQL, which is a web-based IDE. It's a very useful tool for exploring GraphQL operations 
and their responses while developing API. You can think of it as a Swagger for REST APIs.

```
http://localhost:8080/graphiql?query=%7B%0A%20%20%23%20Get%20all%20players%0A%20%20Players%20%7B%0A%20%20%20%20id%0A%20%20%20%20nickname%0A%20%20%20%20email%0A%20%20%20%20firstName%0A%20%20%20%20lastName%0A%20%20%7D%0A%7D
```
Let's start by looking at a very simple query and the result we get when we run it.
At its simplest, GraphQL is about asking for specific fields on objects.
Client query is just a string which is wrapped in json object and send to a server via POST request. 
From the server the client receives a json response with the object he was asking for.

And as we can see client receives exactly that amount of data he was asking about. We can remove some fields from the 
request and observe that amount of data has also been reduced.
 
# Requests
## Requests types
```
Operations(requests) types that can be performed on a server:
```
As you know, when it comes to web applications everything comes down to client-server communication.
So GraphQL specification defines next types of communications between client and server.

```
- query - request to retrieve data 
```
As name suggests, queries are data requests made by the client from the server. 
Unlike to REST where multiple endpoints for data retrieving are defined by a server, GraphQL only discloses single 
endpoint, enabling the client to determine what information it actually requires.

```
- mutation - request to modify data
```
Mutations are used to create, update or delete data. The structure is almost similar to queries except for the fact that
you need to include the word ‘mutation’ in the beginning.

```
- subscription - request to receive data in real time
```
Subscriptions can be used to set up and retain real-time link to the server. This allows you to get instant information 
regarding relevant events. Mostly, a client needs to subscribe to the particular event in order to get the corresponding 
data.

## Queries basic concepts
Most discussions of GraphQL focus on data fetching, so in this presentation I'm going to focus on this type of requests.
```
At its simplest, GraphQL is about asking for specific fields on objects in a various ways.
```

// show demo from the next slide.
```
-Fields and Objects
```
You can see immediately that the query has exactly the same shape as the result. This is essential to GraphQL, 
because you always get back what you expect, and the server knows exactly what fields the client is asking for.
// Teams {...

```
-Arguments
```
In a system like REST, if you want to pass some additional information to your data request, you would use 
the query parameters and URL segments for that. 
But in GraphQL, every field and nested object can get its own set of arguments which looks like method invocations.
// Team(teamid: 1) {...

```
-Aliases
```
Since the result object fields match the name of the field in the query but don't include arguments, you can't directly 
query for the same field with different arguments. That's why you need aliases - they let you rename the result of a 
field to anything you want.
// Team(teamid:2) {...
// whocaresTeam: Team(teamid: 1) {...

```
-Operations names
```
Up until now, we have been using a shorthand syntax where we omit both the query keyword and the query name, 
but in production apps it's useful to use these to make our code less ambiguous. Besides that it comes to handy when it 
comes to logging and bug-fixing your application. 
// query gimmeOneTeamPlease { Team(teamid: 2) {...

```
-Fragments, Variables, Directives and many other cool things...

```
Besides features that were demonstrated there are bunch of other interesting and useful things in GraphQL specification
which may ease client side query construction and thus its communication to a server.

## Requests demo
```
http://localhost:8080/graphiql?query=%7B%0A%09Teams%20%7B%0A%20%20%20%20id%0A%20%20%20%20name%0A%20%20%20%20captain%20%7B%0A%20%20%20%20%20%20id%0A%20%20%20%20%20%20name%3A%20nickname%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D%0A

```

# Type language
## Schemas and Types
```
A GraphQL schema describes the functionality clients can utilize once they connect to the GraphQL server. 
```
So when queries come in to a server, they are validated and executed against that schema.
// show schema file from demo

```
The core building block within schemas is called a type. 
```
Every GraphQL service defines a set of types which completely describe the set of possible data you can get from a 
service. The most basic components of a GraphQL schema are object types, which just represent a kind of object you can 
fetch from your service, and what fields it has.
// show types file from demo

```
Every GraphQL service has a query type and may or may not have a mutation type and object types.
```
Most types in your schema will just be normal object types, but there are two types that are special within a schema, 
those are query and mutation types. These types are the same as a regular object type, but they are special because they 
define the entry point of every GraphQL query.

## Type language syntax
```
Type language is a set of rules which allows GraphQL servers to define schemas and clients to perform their requests 
against those schemas.
```
// show types file from demo
`Team` is a GraphQL Object Type, meaning it's a type with some fields. Object types may contain fields, like `id` or
`name` and other type. To declare type we use `type` keyword. 
When declaring fields we set the name and after colon the type. e.g. `name` field has a `String` type and `captain` 
field has as a type another type object which is `Player`. 
 
String is one of the built-in scalar types - these are types that resolve to a single scalar object, and can't have s
ub-selections in the query.

Exclamation mark means that the field is non-nullable, meaning that the GraphQL service promises to always give you a 
value when you query this field.

Every field on a GraphQL object type can have zero or more arguments
// show schema from demo
For example, `Player` field has `playerId` argument. Arguments are also should have types declared. 
Also for arguments we can define default value if one wasn't provided during query.

Arrays are defined by square brackets.

There are many more features in GraphQL type language like enumeration types, interfaces, union types which are out of 
the scope of this presentation.

[Episode!]! represents an array of Episode objects. Since it is also non-nullable, you can always expect an array (with zero or more items) when you query the appearsIn field. And since Episode! is also non-nullable, you can always expect every item of the array to be an Episode object.
## Schemas and types demo
```
https://gitlab.com/stepanyuk/graphql-spring-boot-demo/blob/master/src/main/resources/queries.graphqls
https://gitlab.com/stepanyuk/graphql-spring-boot-demo/blob/master/src/main/resources/types.graphqls
```

# Comparison with REST
Since REST is the the most popular standard of designing web APIs and GraphQL is seen as its competitor, in this 
presentation I'd like to draw a comparison between those two and show where they are different or similar.
 
## Key ideas
```
REST - data is represented as resources over HTTP through URL.
```
When we are talking about REST APIs we are talking about presenting data as a resources. So each resource has its own 
endpoint and client can manipulate resource by means HTTP protocol. REST makes us use HTTP to the fullest. 

```
GraphQL - client defines response data that it needs via query language.
```
GraphQL relies on HTTP not that much and to consume API you need only one POST endpoint. But at the same time it gives 
clients more freedom in defining how exactly they want to consume API. 

## Key aspects comparison
```
- Data fetching
```
I would say this is the strongest side of GraphQL, because it solves two main issues that are present in REST, those are
data over- and under-fetching.

One of the main issues with REST-ful APIs is the data under-fetching. Because REST treats data as resources 
a client often need to make bunch of requests to assemble all data it requires. For example if we are talking about
some e-commerce web app you can imagine how many resources client need to retrieve from server to display the main page.

On the other hand often with REST, you will end up with data that you don’t need. For example, when calling some 
/user/<id> endpoint, you will get all the data related to the user. But what if we need to get only user name. 
There is no way you can do that and often developers introduce two endpoints like /user/<id> and /user/<id>/details.
This is a classic example of over-fetching in REST.

At the same time while a main benefit of GraphQL is to enable clients to query for just the data they need,
this can also be problematic especially for open APIs where an organization cannot control 3rd party client query 
behavior. Great care has to be taken to ensure GraphQL queries don’t result in expensive join queries that can bring 
down server performance or even DDoS the server.

```
- HTTP usage
```
This is a weak side of GraphQL because it simply doesn't use features and advantages of HTTP protocol and does not 
introduce good alternatives. 
Here I'd like to distinguish two main features of HTTP which are leveraged by REST very successfully.

First one is error management. As you know HTTP provides reach set of error codes which are easy to understand and 
handle. On the contrary GraphQL queries always return a 200 HTTP status code, regardless of whether or not that query 
was successful or not. If your query is unsuccessful, your response JSON will have a top-level errors key with 
associated error messages and stacktrace. This can make it much more difficult to do error handling and can lead to 
additional complexity for things like monitoring.

Another disadvantage is the lack of built-in caching support. Because REST APIs have multiple endpoints, they can 
leverage native HTTP caching to avoid re-fetching resources. With GraphQL, you will need to setup your own caching 
support which means relying on another library or reinventing from scratch.

```
- Complexity
```
If you deal with data that is relatively consistent over time and doesn't imply deep nesting, it's probably better to
stick with REST API. GraphQL is relatively more complex has its own query language for client-server communication.

# Conclusion

GraphQL APIs can be exciting new technology, but it is important to understand the trade-offs before making such 
architectural decisions. Some APIs such as those with very few entities and relationships across entities like analytics 
APIs may not be suited for GraphQL. Whereas applications with many different domain objects like e-commerce where you 
have items, users, orders, payments, and so on may be able to leverage GraphQL much more.
