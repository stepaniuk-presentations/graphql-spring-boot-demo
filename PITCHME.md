---?image=pitchme/img/graphql-logo.png

---
### @color[#e20098](Agenda)

@ol

1. *Introduction*
2. *Requests*
3. *Type Language*
5. *Comparison with REST*
6. *Conclusion*

@olend

---

## @color[#e20098](**Introduction**)

- What is GraphQL
- Brief overview and short demo

---

### @color[#e20098](What is GraphQL)

@snap[text-left]
GraphQL is a query language for APIs and a runtime for fulfilling those queries.
<br/>
<br/>
Developed internally by Facebook in 2012 before being publicly released in 2015. In 2018, the GraphQL project was moved 
from Facebook to the newly-established GraphQL Foundation.
@snapend

---

### @color[#e20098](Brief overview and short demo)

[simple query demo](http://localhost:8080/graphiql?query=%7B%0A%20%20%23%20Get%20all%20players%0A%20%20Players%20%7B%0A%20%20%20%20id%0A%20%20%20%20nickname%0A%20%20%20%20email%0A%20%20%20%20firstName%0A%20%20%20%20lastName%0A%20%20%7D%0A%7D)

---

## @color[#e20098](**Requests**)

- Requests types
- Queries basic concepts
- Requests demo

---

### @color[#e20098](Requests types)

@snap[text-left]
Operations(requests) types that can be performed on a server:
@snapend
<br/>

- **_query_** - request to retrieve data 
- **_mutation_** - request to modify data 
- **_subscription_** - request to receive data in real time

---

### @color[#e20098](Queries basic concepts)

@snap[text-left]
At its simplest, GraphQL is about asking for specific fields on objects in a various ways.
@snapend
<br/>

@ul

- *Fields and Objects*
- *Arguments*
- *Aliases*
- *Operations names*
- *Fragments, Variables, Directives and many other cool things...*

@ulend

---

### @color[#e20098](Requests demo)

[detailed query demo](http://localhost:8080/graphiql?query=%7B%0A%09Teams%20%7B%0A%20%20%20%20id%0A%20%20%20%20name%0A%20%20%20%20captain%20%7B%0A%20%20%20%20%20%20id%0A%20%20%20%20%20%20name%3A%20nickname%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D%0A)

---

## @color[#e20098](**Type Language**)

- Schemas and Types
- Type language syntax
- Schemas and Types demo

---

### @color[#e20098](Schemas and Types)

@snap[text-left]
A GraphQL schema describes the functionality clients can utilize once they connect to the GraphQL server.
<br/>
The core building block within schemas is called a type. Every GraphQL service has a query type and may or may not have 
a mutation type and object types. 
@snapend

---

### @color[#e20098](Type language syntax)

@snap[text-left]
Type language is a set of rules which allows GraphQL servers to define schemas and clients to perform their requests 
against those schemas. 
@snapend

---

### @color[#e20098](Schema and types demo)

[schema demo](https://gitlab.com/stepanyuk/graphql-spring-boot-demo/blob/master/src/main/resources/queries.graphqls)
<br/>
[types demo](https://gitlab.com/stepanyuk/graphql-spring-boot-demo/blob/master/src/main/resources/types.graphqls)

---

## @color[#e20098](**Comparison with REST**)

- Key ideas
- Key aspects comparison

---

### @color[#e20098](Key ideas)

@snap[text-left]
**REST** - _data is represented as resources over HTTP through URL._
<br/>
<br/>
**GraphQL** - _client defines response data that it needs via query language._ 
@snapend

---

### @color[#e20098](Key aspects comparison)
@ul

- *Data fetching*
- *HTTP usage*
- *Complexity*

@ulend

---

## @color[#e20098](**Conclusion**)

---
@snap[midpoint]
![Choose wisely](pitchme/img/choose-wisely-you-must.jpg)
@snapend

---

## Useful Links:

- [GraphQL official website](https://graphql.org)
- [Java library for building GraphQL APIs](https://github.com/graphql-java/graphql-java)
- [JavaScript library for building GraphQL APIs](https://www.npmjs.com/package/graphql)
- [GraphiQL. Interactive in-browser GraphQL IDE](https://github.com/graphql/graphiql)
