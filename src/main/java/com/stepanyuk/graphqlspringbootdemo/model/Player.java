package com.stepanyuk.graphqlspringbootdemo.model;

import lombok.Builder;
import lombok.Data;

@Data @Builder
public class Player {

    private long id;
    private String nickname;
    private String email;
    private String firstName;
    private String lastName;
    private int age;
    private boolean isProfessional;

    private Achievements achievements;
}
