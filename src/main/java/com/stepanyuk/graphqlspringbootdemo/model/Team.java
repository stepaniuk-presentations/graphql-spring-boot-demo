package com.stepanyuk.graphqlspringbootdemo.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data @Builder
public class Team {

    private long id;
    private String name;

    private Player captain;
    private List<Player> players;
}
