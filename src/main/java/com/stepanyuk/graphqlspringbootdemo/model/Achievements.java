package com.stepanyuk.graphqlspringbootdemo.model;

import lombok.Builder;
import lombok.Data;

@Data @Builder
public class Achievements {

    private int championships;
    private int seasonMvp;
    private int finalsMvp;
}
