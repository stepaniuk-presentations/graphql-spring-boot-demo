package com.stepanyuk.graphqlspringbootdemo;

import com.stepanyuk.graphqlspringbootdemo.model.Achievements;
import com.stepanyuk.graphqlspringbootdemo.model.Player;
import com.stepanyuk.graphqlspringbootdemo.model.Team;
import com.stepanyuk.graphqlspringbootdemo.repository.PlayerRepository;
import com.stepanyuk.graphqlspringbootdemo.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class DataLoader {

    private TeamRepository teamRepository;
    private PlayerRepository playerRepository;

    @Autowired
    public DataLoader(TeamRepository teamRepository, PlayerRepository playerRepository) {
        this.teamRepository = teamRepository;
        this.playerRepository = playerRepository;

        loadData();
    }

    private void loadData() {

        Team bulls = Team.builder().id(1).name("Chicago Bulls").build();
        Team lakers = Team.builder().id(2).name("Los Angeles Lakers").build();

        Player jordan = Player.builder().id(1).nickname("Air Jordan").email("mj@gmial.com").age(30).firstName("Michael").lastName("Jordan").isProfessional(true).build();
        Player rose = Player.builder().id(2).nickname("D Rose").email("dr@gmial.com").age(25).firstName("Derrick").lastName("Rose").isProfessional(true).build();
        Player kobe = Player.builder().id(3).nickname("Black Mamba").email("kb@gmial.com").age(28).firstName("Kobe").lastName("Bryant").isProfessional(true).build();
        Player lebron = Player.builder().id(4).nickname("King").email("lj@gmial.com").age(26).firstName("Lebron").lastName("James").isProfessional(true).build();
        Player shaq = Player.builder().id(5).nickname("Shaq").email("so@gmial.com").age(29).firstName("Shaquille").lastName("O Neal").isProfessional(true).build();
        Player alex = Player.builder().id(6).nickname("Alex").email("as@gmial.com").age(28).firstName("Aleksey").lastName("Stepanyuk").isProfessional(false).build();

        Achievements mja = Achievements.builder().championships(6).finalsMvp(3).seasonMvp(4).build();
        Achievements dra = Achievements.builder().championships(0).finalsMvp(0).seasonMvp(1).build();
        Achievements kba = Achievements.builder().championships(5).finalsMvp(2).seasonMvp(1).build();
        Achievements lja = Achievements.builder().championships(3).finalsMvp(3).seasonMvp(4).build();
        Achievements soa = Achievements.builder().championships(4).finalsMvp(3).seasonMvp(1).build();
        Achievements asa = Achievements.builder().championships(0).finalsMvp(0).seasonMvp(0).build();

        jordan.setAchievements(mja);
        rose.setAchievements(dra);
        kobe.setAchievements(kba);
        lebron.setAchievements(lja);
        shaq.setAchievements(soa);
        alex.setAchievements(asa);

        bulls.setCaptain(jordan);
        lakers.setCaptain(kobe);

        bulls.setPlayers(Stream.of(jordan, rose).collect(Collectors.toList()));
        lakers.setPlayers(Stream.of(kobe, lebron, shaq).collect(Collectors.toList()));

        teamRepository.setTeamStorage(Stream.of(bulls, lakers).collect(Collectors.toList()));
        playerRepository.setPlayerStorage(Stream.of(jordan, rose, kobe, lebron, shaq, alex).collect(Collectors.toList()));
    }
}
