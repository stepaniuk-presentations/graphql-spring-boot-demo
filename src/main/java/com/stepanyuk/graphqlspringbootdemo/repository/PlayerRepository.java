package com.stepanyuk.graphqlspringbootdemo.repository;

import com.stepanyuk.graphqlspringbootdemo.model.Player;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PlayerRepository {

    private List<Player> playerStorage;

    @PostConstruct
    private void postConstruct() {
        playerStorage = new ArrayList<>();
    }

    public List<Player> findAll() {
        return playerStorage;
    }

    public Player save(Player player) {
        playerStorage.add(player);

        return player;
    }

    public Player findOne(Long id) {
        List<Player> result = playerStorage.stream().filter(player -> player.getId() == id).collect(Collectors.toList());

        return result.get(0);
    }

    public void setPlayerStorage(List<Player> playerStorage) {
        this.playerStorage = playerStorage;
    }
}
