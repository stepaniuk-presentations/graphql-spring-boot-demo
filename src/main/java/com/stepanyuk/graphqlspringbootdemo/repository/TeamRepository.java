package com.stepanyuk.graphqlspringbootdemo.repository;

import com.stepanyuk.graphqlspringbootdemo.model.Team;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TeamRepository {

    private List<Team> teamStorage;

    @PostConstruct
    private void postConstruct(){
        teamStorage = new ArrayList<>();
    }

    public List<Team> findAll() {
        return teamStorage;
    }

    public Team save(Team team) {
        teamStorage.add(team);

        return team;
    }

    public Team findOne(Long id) {
        List<Team> result = teamStorage.stream().filter(team -> team.getId() == id).collect(Collectors.toList());

        return result.get(0);
    }

    public void setTeamStorage(List<Team> teamStorage) {
        this.teamStorage = teamStorage;
    }
}
