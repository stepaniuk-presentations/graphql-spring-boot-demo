package com.stepanyuk.graphqlspringbootdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphQLSpringBootDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(GraphQLSpringBootDemoApplication.class, args);
    }

}
