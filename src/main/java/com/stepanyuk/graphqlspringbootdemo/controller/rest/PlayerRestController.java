package com.stepanyuk.graphqlspringbootdemo.controller.rest;

import com.stepanyuk.graphqlspringbootdemo.model.Player;
import com.stepanyuk.graphqlspringbootdemo.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/rest")
public class PlayerRestController {

    @Autowired
    private PlayerRepository playerRepository;

    @GetMapping("/players")
    public ResponseEntity<List<Player>> getAllPlayers() {
        return ResponseEntity.ok().body(playerRepository.findAll());
    }

    @PostMapping("/players")
    public ResponseEntity<Player> postPlayer(@RequestBody Player player) throws URISyntaxException {
        return ResponseEntity.created(new URI("/rest/players/" + player.getId())).body(playerRepository.save(player));
    }

    @GetMapping("/players/{playerId}")
    public ResponseEntity<Player> getPlayer(@PathVariable Long playerId) {
        return ResponseEntity.ok().body(playerRepository.findOne(playerId));
    }
}
