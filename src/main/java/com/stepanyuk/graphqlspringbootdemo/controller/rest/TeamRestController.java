package com.stepanyuk.graphqlspringbootdemo.controller.rest;

import com.stepanyuk.graphqlspringbootdemo.model.Team;
import com.stepanyuk.graphqlspringbootdemo.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/rest")
public class TeamRestController {

    @Autowired
    private TeamRepository teamRepository;

    @GetMapping("/teams")
    public ResponseEntity<List<Team>> getAllTeams() {
        return ResponseEntity.ok().body(teamRepository.findAll());
    }

    @PostMapping("/teams")
    public ResponseEntity<Team> postTeam(@RequestBody Team team) throws URISyntaxException {
        return ResponseEntity.created(new URI("/rest/teams/" + team.getId())).body(teamRepository.save(team));
    }

    @GetMapping("/teams/{teamId}")
    public ResponseEntity<Team> getTeam(@PathVariable Long teamId) {
        return ResponseEntity.ok().body(teamRepository.findOne(teamId));
    }
}
