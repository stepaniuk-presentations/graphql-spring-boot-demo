package com.stepanyuk.graphqlspringbootdemo.controller.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.stepanyuk.graphqlspringbootdemo.model.Player;
import com.stepanyuk.graphqlspringbootdemo.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlayerQueryController implements GraphQLQueryResolver {

    @Autowired
    private PlayerRepository playerRepository;

    public List<Player> getPlayers() {
        return playerRepository.findAll();
    }

    public Player getPlayer(long playerId) {
        return playerRepository.findOne(playerId);
    }
}
