package com.stepanyuk.graphqlspringbootdemo.controller.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.stepanyuk.graphqlspringbootdemo.model.Team;
import com.stepanyuk.graphqlspringbootdemo.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamQueryController implements GraphQLQueryResolver {

    @Autowired
    private TeamRepository teamRepository;

    public List<Team> getTeams() {
        return teamRepository.findAll();
    }

    public Team getTeam(long teamId) {
        return teamRepository.findOne(teamId);
    }
}
